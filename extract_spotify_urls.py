import os
import requests
import argparse
import subprocess
import json

def extract_spotify_urls_and_download(download_dir):
    lidarr_url = os.getenv("LIDARR_URL", "http://your-lidarr-ip:port")
    api_key = os.getenv("API_KEY", "your-lidarr-api-key")
    endpoint = f"{lidarr_url}/api/v1/wanted/missing?apiKey={api_key}"

    response = requests.get(endpoint)
    data = response.json()

    downloaded_directories = []

    for record in data.get("records", []):
        artist_links = record.get("artist", {}).get("links", [])

        for link in artist_links:
            if link.get("name") == "spotify":
                spotify_url = link.get("url")
                print(f"Spotify URL for artist {record['artist']['artistName']}: {spotify_url}")

                # Download track using SpotDL
                download_command = f"spotdl {spotify_url} --output {download_dir}/{record['artist']['artistName']}"

                try:
                    subprocess.run(download_command, shell=True, check=True)
                    downloaded_directory = f"{download_dir}/{record['artist']['artistName']}"
                    downloaded_directories.append(downloaded_directory)
                    print(f"Downloaded track for artist {record['artist']['artistName']}")
                except subprocess.CalledProcessError as e:
                    print(f"Error downloading track for artist {record['artist']['artistName']}: {e}")

    # Print the downloaded directories as JSON to stdout
    print(json.dumps(downloaded_directories))

# Parse command line arguments
parser = argparse.ArgumentParser(description='Extract Spotify URLs and download with SpotDL')
parser.add_argument('--download-dir', type=str, default='/downloads/music', help='Download directory')
args = parser.parse_args()

# Run extraction and download
extract_spotify_urls_and_download(args.download_dir)
