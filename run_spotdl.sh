#!/bin/sh

# Run SpotDL with Lidarr API and capture the output
DOWNLOAD_DIR=/downloads/music  # Set your desired download directory
downloaded_directories=$(python /app/extract_spotify_urls.py --download-dir $DOWNLOAD_DIR)

# Process the downloaded directories as needed
echo "Downloaded directories: $downloaded_directories"

# Pass the list of downloaded directories to the second Python script
python /app/import_downloaded_tracks.py "$downloaded_directories"
