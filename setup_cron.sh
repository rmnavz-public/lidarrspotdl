#!/bin/sh

# Set up a cron job to run the run_spotdl.sh script at a specified interval
echo "0 */24 * * * /bin/sh /app/run_spotdl.sh" > /etc/crontab
crontab /etc/crontab

# Start cron in the foreground
cron -f
